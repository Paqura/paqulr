const fs = require('fs');
const path = require('path');
const babel = require('babel-core');
const babylon = require('babylon');
const traverse = require('babel-traverse').default;

/**
 * Создаём константы путей, где будет лежать
 * финальный файл
 */

const distDir = './dist';
const bundleFileName = 'bundle.js';
const bundleName = `${distDir}/${bundleFileName}`;

/**
 * ID создаётся для того, чтобы
 * найденные модули имели уникальный идентификатор,
 * для чего при появлении нового модуля,
 * мы инкрементируем ID
 */

let ID = 0;

const createAsset  = (filename) => {
  /**
   * Читаем переданный файл,
   * обязательно выставить кодировку utf-8
   */

  const content = fs.readFileSync(filename, 'utf-8');

  /**
   * babylon распарсит полученный контент, 
   * который представляет из себя бинарник
   */

  const ast = babylon.parse(content, {
    sourceType: 'module'
  });

  const dependencies = [];

  /**
   * traverse ищет импорты
   * и пушит, если такие найдены,
   * в массив зависимостей
   */

  traverse(ast, {
    ImportDeclaration: ({ node }) => {
      dependencies.push(node.source.value);
    } 
  });
  
  const id = ID++;

  /**
   * транпайлим код в es5
   */

  const { code } = babel.transformFromAst(ast, null, {
    presets: ['env']
  });

  return {
    id,
    filename,
    dependencies,
    code
  }
};

/**
 * @param entry Путь к главному файлу
 * @return {Array} Возвращает массив-очередь
 */

const createGraph = (entry) => {
  /**
   * создаём входную точку и записываем её 
   * первой в очеред queue
   */

  const mainAsset = createAsset(entry);
  const queue = [mainAsset];

  /**
   * заходим циклом в очередь и назначаем 
   * имя директории, где лежит файл
   * (в данном примере './javascript')
   */

  for(const asset of queue) {
    const dirname = path.dirname(asset.filename);
    
    /**
     * создаём дополнительное поле mapping,
     * в котором будут или не будут, если их нет,
     * храниться дочерние модули, подключаемые через import
     */

    asset.mapping = {};

    /**
     * заходим в зависимости нашего ассета,
     * каждая зависимость это относительный путь,
     * т.е. в он ничего не знает, в какой папке находится,
     * в нашем примере, все пути начинаются с './';
     * мы создаём absolutePath, добавляя к относительному пути 
     * директорию родителя.
     * Далее создаём ребенка, передавая правильный путь
     * эта строка - const child = createAsset(absolutePath);
     * в asset.mapping мы добавляем в поле относительного пути - id "ребёнка",
     * что даём нам понимание, где лежит определённая зависимость
     */

    asset.dependencies.forEach(relativePath => {
      const absolutePath = path.join(dirname, relativePath + '.js');     
      const child = createAsset(absolutePath);

      asset.mapping[relativePath] = child.id;
      queue.push(child);
    });
  }

  return queue;
};

/**
 * @param  dependeciesGraph Граф зависимостей нашего приложения
 * @return resultString Собственно наш скомпилинный javascript,
 * который в последствии запишется в файл
 */

const bundle = (dependeciesGraph) => {
  let modules = '';

  /** Мы используем id модуля в качестве ключа и массив в качестве значения
    * (в нем будет 2 элемента для каждого модуля)    
    * Первое значение это транспилированный через Babel исходный код, обернутый
    * в функцию. Это сделано для изоляции области видимости каждого модуля, ведь
    * они между собой и тем более глобальной областью видимости, не должны пересекаться.
    *
    * Наши модули после трансрпиляции будут в формате модулей CommonJS. Они будут ожидать
    * передачи через аргументы функции require и объектов module и exports.
    * В браузерах нативно такой функции нет, потому мы сами пишем ее и внедряем в обертки модулей.
    *
    * Второе значение это преобразованный в строку объект mapping, содержащий информацию
    * о связи данного модуля с его зависимостями в таком виде:
    * { './relative/path': 1 }
    *
    * Дело в том, что транспилированный код модулей содержит вызовы с подключением
    * через `require()` с относительными путями в качестве аргумента. Так что мы должны
    * знать какой модуль в графе какому относительному пути соответствует.
  **/
  dependeciesGraph.forEach(mod => {
    modules += `${mod.id}: [
      function(require, module, exports) { 
        ${mod.code} 
      },
      ${JSON.stringify(mod.mapping)},
    ],`;
  });

  /** Ну и нацонец, напишем тело нашей самовызывающейся функции.
  *
  * Начнем с функции `require()`. Она принимает id модуля в качестве аругумента,
  * ища его на объекте modules, который мы передали при вызове.
  *
  * Деструктурируем массив извлекая два наших значения, содержащих код функции
  * модуля в обертке и объект mapping.
  *
  * Код наших модулей вызывает `require()` с относительным путем. вместо id нужного модуля.
  * Кроме того, два модуля могут зареквайрить один и тот же относительный путь,
  * который будет указывать на разные файлы в переложении на абсолютные пути.
  *
  * Чтобы грамотно обработать эту ситуацию, при подключении модуля, мы создаем новую,
  * локальную функцию `require()` для подключения именно этого модуля. Она будет знать,
  * как превратить относительный код данного модуля в его реальный id, по которому его можно
  * будет взять. Именно для этого и нужен объект mapping, содержащий отношения между
  * относительными путями к зависимостям данного модуля и реальными id.
  *
  * Наконец, когда модуль зареквайрен, он может изменить объект exports. После изменения кодом
  * модуля, этот объект возвращается из функции `require()`
  **/

  const resultString = `
    (function(modules) {
      function require(id) {
        const [fn, mapping] = modules[id];

        function localRequire(relativePath) {
          return require(mapping[relativePath])
        }

        const module = { exports: {} };

        fn(localRequire, module, module.exports);

        return module.exports;
      }

      require(0);
    })({ ${modules} })
  `;
  writeFile(resultString);
  return resultString;
};

const graph = createGraph('./javascript/index.js');
const result = bundle(graph);

/**
 * Если директории нет, то создаём её + создаём наш бандл
 * иначе просто переписываем содержимое файла
 */

function writeFile (result) {
  if(!fs.existsSync(distDir)) {
    fs.mkdirSync(distDir);   
  }
  fs.writeFile(bundleName, result,  (err) => {
    if(err) throw err;
    console.log('file saved');
  })
}

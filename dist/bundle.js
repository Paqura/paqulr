
    (function(modules) {
      function require(id) {
        const [fn, mapping] = modules[id];

        function localRequire(relativePath) {
          return require(mapping[relativePath])
        }

        const module = { exports: {} };

        fn(localRequire, module, module.exports);

        return module.exports;
      }

      require(0);
    })({ 0: [
      function(require, module, exports) { 
        "use strict";

var _name = require("./name");

var _message = require("./message");

(0, _message.message)(_name.name); 
      },
      {"./name":1,"./message":2},
    ],1: [
      function(require, module, exports) { 
        "use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var name = exports.name = 'Paqulr bundler test'; 
      },
      {},
    ],2: [
      function(require, module, exports) { 
        "use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var message = exports.message = function message(name) {
  console.log(name);
}; 
      },
      {},
    ], })
  